package com.anas.channelservice.config;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachePut;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class ChannelServiceHystrix {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CacheManager cacheManager;

    @HystrixCommand(fallbackMethod = "listChannelsFallback")
    public List<String> listChannels(RequestEntity requestEntity) {
        HttpEntity httpEntity = new HttpEntity(requestEntity.getBody(), requestEntity.getHeaders());

        val response = restTemplate.exchange("http://db-service/channel/list",
                HttpMethod.GET, httpEntity,
                List.class);


        return response.getBody();
    }

    public List<String> listChannelsFallback(RequestEntity requestEntity) {
        return new ArrayList<>();
    }

    @HystrixCommand(fallbackMethod = "fetchMessagesFallback")
    @CachePut("channels")
    public List fetchMessages(RequestEntity requestEntity) {
        val channelQuery = requestEntity.getUrl().getQuery();

        HttpEntity httpEntity = new HttpEntity(requestEntity.getBody(), requestEntity.getHeaders());

        val response = restTemplate.exchange("http://db-service/channel/fetch?" + channelQuery,
                HttpMethod.GET, httpEntity,
                List.class);

        return response.getBody();
    }

    public List fetchMessagesFallback(RequestEntity requestEntity) {
        Cache.ValueWrapper w = cacheManager.getCache("channels").get(requestEntity);
        if (w != null)
            return (List) w.get();
        else
            return new ArrayList<>();

    }
}
