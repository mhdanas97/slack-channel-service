package com.anas.channelservice.config;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient("db-service")
public interface DbService {
//   # @PostMapping("/channel")
//    public String addChannel(Object channel);

    @PostMapping("/users/sign-up")
    public String signUp(Object user);
}
