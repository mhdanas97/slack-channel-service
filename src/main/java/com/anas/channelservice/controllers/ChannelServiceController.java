package com.anas.channelservice.controllers;

import com.anas.channelservice.config.ChannelServiceHystrix;
import com.anas.channelservice.config.DbService;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/")
public class ChannelServiceController {
    private static final Logger log = LoggerFactory.getLogger(ChannelServiceController.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private DbService dbService;

    @Autowired
    private ChannelServiceHystrix channelService;

    @PostMapping("/")
    public String addChannel(RequestEntity requestEntity) {

        HttpEntity httpEntity = new HttpEntity(requestEntity.getBody(), requestEntity.getHeaders());
        val response = restTemplate.exchange("http://db-service/channel",
                HttpMethod.POST, httpEntity,
                String.class);
        log.info("addChannel respond {}", response);
        return response.getBody();
    }

    @PostMapping("/subscribe")
    public String subscribe(RequestEntity requestEntity) {
        val channelName = ((HashMap) requestEntity.getBody()).get("name");
        HttpEntity httpEntity = new HttpEntity(requestEntity.getBody(), requestEntity.getHeaders());

        val response = restTemplate.exchange("http://db-service/channel/subscribe?channel=" + channelName,
                HttpMethod.POST, httpEntity,
                String.class);

        log.info("subscribe to channel {} respond {}", channelName, response);
        return response.getBody();
    }

    @GetMapping("/fetch")
    public List fetch(RequestEntity requestEntity) {
        List messages = channelService.fetchMessages(requestEntity);

        log.info("fetch messages of channel {}", messages.size());
        return messages;
    }

    @GetMapping("/list")
    public List<String> listChannels(RequestEntity requestEntity) {
        List<String> channels = channelService.listChannels(requestEntity);

        log.info("fetch channels list {}", channels.size());
        return channels;
    }
}
